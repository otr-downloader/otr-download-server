package core.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

public class Config {
private Properties config;
	
	private String path = System.getProperty("user.dir")+File.separator+"config"+File.separator+"otrdownloader-server.properties";
	
	public Config(){
		config = new Properties();
		
		if(!new File(System.getProperty("user.dir")+File.separator+"config").exists()){
			new File(System.getProperty("user.dir")+File.separator+"config").mkdir();
		}
		if(!new File(path).exists() || !new File(System.getProperty("user.dir")+File.separator+"config"+File.separator+"otrdownloader-otrkeys.txt").exists()){
			try {
				new File(path).createNewFile();
				new File(System.getProperty("user.dir")+File.separator+"config"+File.separator+"otrdownloader-otrkeys.txt").createNewFile();
				new File(System.getProperty("user.dir")+File.separator+"config"+File.separator+"hdsender.txt").createNewFile();
				save("logdir", System.getProperty("user.dir")+File.separator+"log");
				save("decodemanualdir",System.getProperty("user.dir"));
				save("ftpip","mediaftp.ddns.net");
				save("ftpactive","false");
				save("debug","false");
				save("maxconnections", "4");
				System.out.println("Config einstellen !!!");
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			config.load(new FileInputStream(new File(path)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String load(String key){
		return config.getProperty(key);
	}
	
	public void save(String key,String wert){
		config.setProperty(key, wert);
		try {
			config.store(new FileOutputStream(new File(path)), "OTR Downloader Config");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Vector<String> DateiAuslesen(String path){
		Vector<String> names = new Vector<String>();
		
		File file = new File(path);
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			
			String text;
			while((text = reader.readLine())!= null){
				names.add(text);
			}
			
			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return names;
	}
}
