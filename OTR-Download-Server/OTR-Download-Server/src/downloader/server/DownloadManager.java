package downloader.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import core.main.Debug;
import core.main.OTRDServer;
import finder.core.OtrKeyResult;

public class DownloadManager implements Runnable {

	private Vector<OtrKeyResult> downloads;

	public DownloadManager() {
		downloads = new Vector<OtrKeyResult>();
	}

	@Override
	public void run() {
		while (true) {
			//Debug.println("Download-Manager Schleife startet");
			Vector<OtrKeyResult> tmp = new Vector<OtrKeyResult>(downloads);
			Haupt:
			for (OtrKeyResult result : tmp) {
				Vector<Connection> ctmp = new Vector<Connection>(OTRDServer.getThreadManager().getDLServer().getConnections());
				Collections.shuffle(ctmp);
				for (Connection c : ctmp) {
					if (c.isAvailable()) {
						c.startDownload(result);
						downloads.remove(result);
						continue Haupt;
					}
				}
			}
			//Debug.println("Download-Manager Schleife beendet");
			//CPU Entlastung
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void addDownload(OtrKeyResult result) {
		OTRDServer.getLog().println("DownloadManager : "+result.getOtrkey()+" hinzugefügt");
		downloads.add(result);
	}
	
	public Vector<OtrKeyResult> getDownloads(){
		return downloads;
	}

}
