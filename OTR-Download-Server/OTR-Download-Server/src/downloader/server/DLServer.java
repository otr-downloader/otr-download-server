package downloader.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

import core.main.Debug;
import core.main.OTRDServer;
import finder.core.OtrKeyResult;

public class DLServer implements Runnable{

	private SSLServerSocket serverSocket;
	private Vector<Connection> connections;
	private ExecutorService executorService;
	private int maxconnections;
	
	public DLServer() {
		//Max Connections to Server
		maxconnections = Integer.parseInt(OTRDServer.getConf().load("maxconnections"));
		//Standart Initializing
		connections = new Vector<Connection>();
		//Executor Service
		executorService = Executors.newFixedThreadPool(maxconnections);
		//Keystore Settings
		//System.setProperty("javax.net.ssl.keyStore", "keystore");
	    //System.setProperty("javax.net.ssl.keyStorePassword", "31177#160799");
	}
	
	@Override
	public void run() {
		try {
			SSLServerSocketFactory sslserversocketfactory;
			
			SSLContext ctx;
            KeyManagerFactory kmf;
            KeyStore ks;
            char[] passphrase = "31177#160799".toCharArray();

            ctx = SSLContext.getInstance("TLS");
            kmf = KeyManagerFactory.getInstance("SunX509");
            ks = KeyStore.getInstance("JKS");

            ks.load(new FileInputStream("keystore"), passphrase);

            kmf.init(ks, passphrase);
            ctx.init(kmf.getKeyManagers(), null, null);

            sslserversocketfactory = ctx.getServerSocketFactory();
			
			
			//sslserversocketfactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
			
			serverSocket = (SSLServerSocket) sslserversocketfactory.createServerSocket(60);
			System.out.println("Server gestartet");
			while(true) {
				SSLSocket s = (SSLSocket) serverSocket.accept();
				Connection c = new Connection(s);
				executorService.execute(c);
				connections.addElement(c);
				Debug.println("Neue Verbindung von "+s.getInetAddress().toString());
				Debug.println("Aktuelle Anzahl von verbunden Clients : "+connections.size());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void connlost(Connection c) {
		OTRDServer.getThreadManager().getDLBackup().put(c);
		connections.remove(c);
	}
	
	public Vector<Connection> getConnections(){
		return connections;
	}
	
}
