package downloader.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.util.Date;
import java.util.UUID;
import java.util.Vector;

import javax.net.ssl.SSLSocket;

import core.main.Debug;
import core.main.OTRDServer;
import finder.core.OtrKeyResult;

public class Connection implements Runnable{

	private Vector<OtrKeyResult> otrkeys;
	
	private int maxop;
	
	//Server Variablen
	
	private SSLSocket socket;
	private PrintWriter printWriter;
	private String ip;
	private Date date;
	private UUID uuid;
	private boolean available;
	
	
	public Connection(SSLSocket s) {
		available = false;
		ip = s.getInetAddress().getHostAddress();
		maxop = 0;
		uuid = UUID.randomUUID();
		otrkeys = new Vector<OtrKeyResult>();
		socket = s;
		date = new Date();
		try {
			socket.setSoTimeout(30000);
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
		try {
			printWriter = new PrintWriter(socket.getOutputStream(),true);
		} catch (IOException e) {
			e.printStackTrace();
			OTRDServer.getThreadManager().getDLServer().connlost(this);
		}
	}


	@Override
	public void run() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
			String line = null;
			while((line = reader.readLine())!= null) {
				if(line.startsWith("MESSAGE:")){
					String msg = line.split("MESSAGE:")[1];
					if(!msg.equals("Test")) {
						Debug.println("Lese Nachricht : "+msg);
					}
					resolve(line.split("MESSAGE:")[1]);
				}
			}
			maxop = 0;
			System.out.println("Verbindung zu Client ("+ip+") verloren");
			//Verbindung verloren
			OTRDServer.getThreadManager().getDLServer().connlost(this);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			maxop = 0;
			System.out.println("Verbindung zu Client ("+ip+") verloren");
			e.printStackTrace();
			//Verbindung verloren
			OTRDServer.getThreadManager().getDLServer().connlost(this);
		}
	}
	
	/**
	 * Starten den Download anhand des OtrKeyResult
	 * @param result
	 */
	public void startDownload(OtrKeyResult result) {
		Debug.println("Client "+socket.getInetAddress().toString()+" : Starte Download "+result.getOtrkey());
		otrkeys.add(result);
		send("download!start!"+result.asString());
	}
	
	private void resolve(String msg) {
		String[] split = msg.split("!");
		switch(split[0]) {
		case "version":
			send("version!"+OTRDServer.version);
			send("uuid!"+uuid.toString());
			break;
		case "ready":
			available = true;
			break;
		case "busy":
			available = false;
			break;
		case "maxop":
			maxop = Integer.parseInt(split[1]);
			Debug.println("Max Operations ("+ip+") : "+maxop);
			break;
		case "finished":
			Vector<OtrKeyResult> otResults = new Vector<OtrKeyResult>(otrkeys);
			for(int i = 0;i<otResults.size();i++) {
				if(otResults.elementAt(i).getOtrkey().contains(split[1])) {
					otrkeys.removeElement(otResults.elementAt(i));
					System.out.println("Download "+split[1]+" fertiggestellt");
					OTRDServer.getLog().println("Download : "+split[1] + " fertiggestellt");
				}
			}
			break;
		case "retry":
			Vector<OtrKeyResult> otResults2 = new Vector<OtrKeyResult>(otrkeys);
			for(int i = 0;i<otResults2.size();i++) {
				if(otResults2.elementAt(i).getOtrkey().equals(split[1])) {
					otrkeys.removeElement(otResults2.elementAt(i));
					OTRDServer.getThreadManager().getDownloadManager().addDownload(otResults2.elementAt(i));
					Debug.println("Download-Retry : "+split[1]);
				}
			}
			break;
		case "mkey":
			OTRDServer.getThreadManager().getOtrKeyFinder().addManualkey(split[1]);
			break;
		case "getuuid":
			Vector<OtrKeyResult> otrkeys = OTRDServer.getThreadManager().getDLBackup().get(UUID.fromString(split[1])).getOtrkeys();
			if(otrkeys != null) {
				this.otrkeys.addAll(otrkeys);
			}
			break;
		case "Test":
			send("Test");
			break;
		}
	}
	
	public void send(String msg) {
		try {
			if(!msg.equals("Test")) {
				Debug.println("Sende Nachricht : "+msg);
			}
			printWriter.println();
			printWriter.println("MESSAGE:"+msg);
			printWriter.println();
			printWriter.flush();
			socket.getOutputStream().flush();
		} catch (IOException e) {
			maxop = 0;
			e.printStackTrace(OTRDServer.getLog().getWriter());
			System.out.println("Verbindung zu Client ("+ip+") verloren");
			//Verbindung verloren
			OTRDServer.getThreadManager().getDLServer().connlost(this);
		}
	}
	
	public Vector<OtrKeyResult> getOtrkeys(){
		return otrkeys;
	}
	
	public boolean isAvailable() {
		return (maxop > otrkeys.size()) && available;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getIP() {
		return ip;
	}
	
	public UUID getUUID() {
		return uuid;
	}
}
