package core.main;

public class OTRDServer {
	
	private static Config config;
	private static Logger logger;
	private static ThreadManager threadManager;
	
	public static String version = "0.0.1";

	public static void main(String[] args) {
		new OTRDServer();
	}
	
	public OTRDServer() {
		init();
		threadManager = new ThreadManager();
		Debug.println("Debugmodus gestartet");
	}
	
	public OTRDServer(boolean tester) {
		if(!tester) {
			throw new RuntimeException("Wrong constructor");
		}
		init();
		Debug.println("Debugmodus gestartet");
	}
	
	public void init() {
		config = new Config();
		logger = new Logger();
	}

	public static Config getConf() {
		return config;
	}
	
	public static Logger getLog() {
		return logger;
	}
	
	public static ThreadManager getThreadManager() {
		return threadManager;
	}
	
}
