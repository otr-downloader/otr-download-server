package downloader.server;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.Vector;

import core.main.OTRDServer;
import finder.core.OtrKeyResult;

public class DownloadBackup implements Runnable{

	private HashMap<UUID, ConnectionInfo> map;
	
	public DownloadBackup() {
		map = new HashMap<UUID, ConnectionInfo>();
	}
	
	@Override
	public void run() {
		while(true) {
			synchronized (this) {
				Collection<ConnectionInfo> vec = map.values();
				for(ConnectionInfo info : vec) {
					if(info.getDate().before(new Date())) {
						for(OtrKeyResult keyResult : info.getOtrkeys()) {
							OTRDServer.getThreadManager().getDownloadManager().addDownload(keyResult);
						}
						vec.remove(info);
					}
				}
			}
			
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void clearbackup() {
		for(ConnectionInfo cInfo : map.values()) {
			for(OtrKeyResult keyResult : cInfo.getOtrkeys()) {
				OTRDServer.getThreadManager().getDownloadManager().addDownload(keyResult);
			}
		}
		map.clear();
	}
	
	public synchronized void put(Connection c) {
		if(c.getOtrkeys().size() == 0) {
			return;
		}
		//600000L = 10 Minuten
		ConnectionInfo info = new ConnectionInfo(new Date(System.currentTimeMillis()+600000L), c.getOtrkeys());
		map.put(c.getUUID(), info);
	}
	
	public synchronized ConnectionInfo get(UUID id) {
		return map.remove(id);
	}

	class ConnectionInfo{
		private Date date;
		private Vector<OtrKeyResult> otrkeys;
		
		public ConnectionInfo(Date date, Vector<OtrKeyResult> results) {
			this.date = date;
			otrkeys = results;
		}
		
		public Vector<OtrKeyResult> getOtrkeys() {
			return otrkeys;
		}
		
		public Date getDate() {
			return date;
		}
	}
}
