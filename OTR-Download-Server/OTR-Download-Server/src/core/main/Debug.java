package core.main;

public class Debug {

	public static void println(String msg){
		OTRDServer.getLog().println("DEBUG: "+msg);
		if(Boolean.parseBoolean(OTRDServer.getConf().load("debug"))){
			System.out.println(msg);
		}
	}
	
	public static void print(String msg){
		OTRDServer.getLog().print("DEBUG: "+msg);
		if(Boolean.parseBoolean(OTRDServer.getConf().load("debug"))){
			System.out.print(msg);
		}
	}
	
	public static void err(String msg){
		OTRDServer.getLog().println("DEBUG-ERROR: "+msg);
		if(Boolean.parseBoolean(OTRDServer.getConf().load("debug"))){
			System.err.println(msg);
		}
	}
	
}
