package core.main;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import downloader.server.DLServer;
import downloader.server.DownloadBackup;
import downloader.server.DownloadManager;
import finder.core.OTRKeyFinder;

public class ThreadManager {
	
	private DownloadManager downloadManager;
	
	private OTRKeyFinder otrKeyFinder;
	
	private DLServer dlServer;
	
	private DownloadBackup dlBackup;
	
	private ExecutorService executorService;
	
	private Console console;
	
	public ThreadManager() {
		executorService = Executors.newFixedThreadPool(5);
		
		dlServer = new DLServer();
		downloadManager = new DownloadManager();
		dlBackup = new DownloadBackup();
		otrKeyFinder = new OTRKeyFinder();
		console = new Console();
		
		executorService.execute(dlBackup);
		executorService.execute(dlServer);
		executorService.execute(otrKeyFinder);
		executorService.execute(downloadManager);
		executorService.execute(console);
	}
	
	public String getStatus() {
		String status = "OTRKeyFinder Status : "+otrKeyFinder.getLastCheck() + "\n";
		
		return status;
	}

	public DownloadManager getDownloadManager() {
		return downloadManager;
	}

	public OTRKeyFinder getOtrKeyFinder() {
		return otrKeyFinder;
	}
	
	public DLServer getDLServer() {
		return dlServer;
	}
	
	public DownloadBackup getDLBackup() {
		return dlBackup;
	}
	
}
