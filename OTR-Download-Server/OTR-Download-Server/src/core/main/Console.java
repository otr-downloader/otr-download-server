package core.main;

import java.util.Scanner;
import java.util.Vector;

import downloader.server.Connection;
import finder.adv.Suchmuster;
import finder.core.CutlistSearcher;
import finder.core.OTRKeyFinder;
import finder.core.OtrKeyResult;

/**
 * Konsolenklasse
 * 
 * @author konst
 * @version 1.0 (Nov. 2017)
 */
public class Console implements Runnable {

	private Scanner scanner;

	@Override
	public void run() {
		System.out.println("Konsole betriebsbereit");
		while (true) {
			scanner = new Scanner(System.in);
			switch (scanner.nextLine()) {
			case "downloads":
				downloads();
				break;
			case "connections":
				connection(0);
				break;
			case "conkeys":
				connection(2);
				break;
			case "remcon":
				connection(1);
				break;
			case "mkey":
				manualkey(1);
				break;
			case "aufnahmen":
				aufnahmen();
				break;
			case "clear":
				// Im Aufbau
				break;
			case "cbackup":
				clearbackup();
				break;
			case "overviewkey":
				overviewkey();
				break;
			case "addkeys":
				checkkeys();
				break;
			case "addtemplatekeys":
				addtmpaufnahmenmuster();
				break;
			case "status":
				status();
				break;
			case "help":
				help();
				break;
			case "test":
				break;
			default:
				System.out.println("Unknown command");
				break;
			}
		}
	}

	private void help() {
		System.out
				.println("------------------------------------------------------------------------------------------");
		System.out.println("                    OTR-Downloader Version " + OTRDServer.version);
		System.out
				.println("---------------------------------------Commands-------------------------------------------");
		System.out.println("connections	- Zeigt alle Verbindungen an");
		System.out.println("conkeys  	- Zeigt die zugeordneten Keys aller Verbindungen");
		System.out.println("mkey		- F�gt einen OTRKEY manuell hinzu");
		System.out.println("cbackup   	- Leert den DownloadBackup");
		System.out.println("aufnahmen 	- ?");
		System.out.println("status		- Zeigt den Status aller Threads an");
		System.out.println("checkkey	- Check Key");
		System.out
				.println("------------------------------------------------------------------------------------------");
	}

	private void status() {
		System.out.println("-------------------------------Thread-Status-------------------------------");
		System.out.println(OTRDServer.getThreadManager().getStatus());
		System.out.println("---------------------------------------------------------------------------");
	}

	private void overviewkey() {
		System.out.print("OTRKeyFinder Sucheingabe : ");
		String eingabe = scanner.nextLine();
		if (eingabe.equals("exit") || eingabe.equals(""))
			return;
		Vector<String> ausgabe = OTRDServer.getThreadManager().getOtrKeyFinder().searchlistOtrKeys(eingabe);
		if (ausgabe == null) {
			System.out.println("Error : Invalid Input");
			return;
		}

		int id = 0;
		System.out.println("---------------------------------------------------------------------------");
		for (String s : ausgabe) {
			boolean c = false;
			if (CutlistSearcher.getAnzahl(s.split(".otrkey")[0]) > 0) {
				c = true;
			}
			System.out.println(id + ":	" + OTRKeyFinder.formatOTRKey(s) + "	:" + OTRKeyFinder.neutralize(s) + "	:	" + c);
			id++;
		}
		System.out.println("---------------------------------------------------------------------------");
		System.out.print("Welchen Key willst du hinzuf�gen (ID) ? ");
		eingabe = scanner.nextLine();
		if (eingabe.equals("exit"))
			return;
		int sid;
		try {
			sid = Integer.parseInt(eingabe);
		} catch (NumberFormatException e) {
			System.out.println("Not a Number!");
			return;
		}
		OTRDServer.getThreadManager().getDownloadManager()
				.addDownload(OTRDServer.getThreadManager().getOtrKeyFinder().search(ausgabe.get(sid)));

	}

	private void checkkeys() {
		System.out.print("OTRKeyFinder Sucheingabe : ");
		String eingabe = scanner.nextLine();
		if (eingabe.equals("exit") || eingabe.equals(""))
			return;
		String suche = eingabe;
		Vector<String> ausgabe = OTRDServer.getThreadManager().getOtrKeyFinder().searchlistOtrKeys(eingabe);
		if (ausgabe == null) {
			System.out.println("Error : Invalid Input");
			return;
		}
		Vector<String> cstrings = new Vector<>();
		System.out.println("---------------------------------------------------------------------------");
		for (String s : ausgabe) {
			boolean c = false;
			if (CutlistSearcher.getAnzahl(s.split(".otrkey")[0]) > 0) {
				c = true;
				cstrings.add(s);
			}
			System.out.println(OTRKeyFinder.neutralize(s) + "	:	" + c);
		}
		System.out.println("---------------------------------------------------------------------------");
		System.out.print("M�chten sie die Keys hinzuf�gen ? (true|Others Input) ");
		eingabe = scanner.nextLine();
		if (!Boolean.parseBoolean(eingabe))
			return;
		System.out.print("M�chten alle Keys hinzuf�gen ? (true|Others Input) ");
		eingabe = scanner.nextLine();
		if (Boolean.parseBoolean(eingabe)) {
			OTRDServer.getThreadManager().getDownloadManager().getDownloads()
					.addAll(OTRDServer.getThreadManager().getOtrKeyFinder().search(suche, null));
			return;
		}

		System.out.print("M�chten sie alle mit Cutlist hinzuf�gen ? (true|Others Input) ");
		eingabe = scanner.nextLine();
		if (Boolean.parseBoolean(eingabe)) {
			OTRDServer.getThreadManager().getDownloadManager().getDownloads()
					.addAll(OTRDServer.getThreadManager().getOtrKeyFinder().search(cstrings));
			return;
		}
		System.out.println("Kein Key hinzugef�gt");
		// TODO : Erweitern (Spezifische Keys IDS! )
	}

	private void addtmpaufnahmenmuster() {
		Suchmuster suchmuster = new Suchmuster();
		String eingabe;
		System.out.print("Gebe den Suchparameter ein : ");
		eingabe = scanner.nextLine();
		suchmuster.setTitel(eingabe);
		System.out.print("M�chten sie ein Titelpattern hinzuf�gen ? (y/n) ");
		eingabe = scanner.nextLine();
		if (eingabe.equals("y")) {
			System.out.print("Geben sie das Titelpattern ein : ");
			eingabe = scanner.nextLine();
			suchmuster.setTitelpattern(eingabe);
		}
		System.out.print("Gebe das Senderpattern ein : ");
		eingabe = scanner.nextLine();
		suchmuster.setSenderpattern(eingabe);
		System.out.print("Gebe das Startzeitpattern ein : ");
		eingabe = scanner.nextLine();
		suchmuster.setStartzeitpattern(eingabe);
		System.out.print("Gebe den Tag an dem deine Aufnahme l�uft (Montag = 1, Sonntag = 7) : ");
		eingabe = scanner.nextLine();
		suchmuster.setDatumpattern(eingabe);
		System.out.print("Sind die Staffel und Folgenangaben relevant ? (y/n) ");
		eingabe = scanner.nextLine();
		if (eingabe.equals("y")) {
			System.out.print("Gebe das Staffelpattern an : ");
			eingabe = scanner.nextLine();
			suchmuster.setStaffelpattern(eingabe);
			System.out.print("Gebe das Folgenpattern an : ");
			eingabe = scanner.nextLine();
			suchmuster.setFolgepattern(eingabe);
		}

		System.out.print("Alle Angaben korrekt (finish) ? ");
		eingabe = scanner.nextLine();
		if (!eingabe.equals("finish"))
			return;

		Vector<String> keys = OTRDServer.getThreadManager().getOtrKeyFinder().searchlistOtrKeys(suchmuster.getTitel());

		Vector<String> result = new Vector<>();
		Vector<String> cstrings = new Vector<>();

		System.out.println("---------------------------------------------------------------------------");

		for (String s : keys) {
			if (suchmuster.equalsSuchmuster(s)) {
				boolean c = false;
				if (CutlistSearcher.getAnzahl(s.split(".otrkey")[0]) > 0) {
					c = true;
					cstrings.add(s);
				}
				result.add(s);

				System.out.println(OTRKeyFinder.neutralize(s) + "	" + c);
			}
		}

		System.out.println("---------------------------------------------------------------------------");

		System.out.print("M�chten sie die Keys hinzuf�gen (true|Others Input) : ");
		eingabe = scanner.nextLine();
		if (!Boolean.parseBoolean(eingabe))
			return;
		System.out.print("M�chten alle Keys hinzuf�gen (true|Others Input) : ");
		eingabe = scanner.nextLine();
		if (Boolean.parseBoolean(eingabe)) {
			OTRDServer.getThreadManager().getDownloadManager().getDownloads()
					.addAll(OTRDServer.getThreadManager().getOtrKeyFinder().search(result));
			return;
		}

		System.out.print("M�chten sie alle mit Cutlist hinzuf�gen (true|Others Input) ?");
		eingabe = scanner.nextLine();
		if (Boolean.parseBoolean(eingabe)) {
			OTRDServer.getThreadManager().getDownloadManager().getDownloads()
					.addAll(OTRDServer.getThreadManager().getOtrKeyFinder().search(cstrings));
			return;
		}
		System.out.println("Kein Key hinzugef�gt");
	}

	private void addpermaufnahmenmuster() {

	}

	private void clearbackup() {
		OTRDServer.getThreadManager().getDLBackup().clearbackup();
		System.out.println("Backup cleared");
	}

	private void downloads() {
		System.out.println("---------------------------------------------------------------------------");
		for (OtrKeyResult r : OTRDServer.getThreadManager().getDownloadManager().getDownloads())
			System.out.println(r.getOtrkey());
		System.out.println("---------------------------------------------------------------------------");
	}

	/**
	 * Noch nicht fertig geplannt
	 */
	private void clear() {
		System.out.println("Welche Downloads wollen sie clearen");
		System.out.print("Eingabe: ");
		switch (scanner.nextLine()) {
		case "Verbindung":
			connection(0);
			Vector<Connection> cons = OTRDServer.getThreadManager().getDLServer().getConnections();
			System.out.print("Welche Verbindung wollen sie clearen ?");
			String nextline = scanner.nextLine();
			if (nextline.equals("exit")) {
				return;
			}
			try {
				int id = Integer.parseInt(nextline);
				if (id < 0 | id > cons.size() - 1) {
					System.out.println("Fehler : Connection ID existiert nicht");
					return;
				}

				System.out.println("--------------------------------------------------");
				int count = 0;
				for (OtrKeyResult keyResult : cons.get(id).getOtrkeys()) {
					System.out.println("\t" + count + "\t" + keyResult.getOtrkey());
				}
				System.out.println("--------------------------------------------------");
				System.out.println("Bitte geben sie die zu clearenden OTRKEYS an Bsp: 1-4,2,all ");

				String eingabe = scanner.nextLine();
				if (eingabe.contains("all")) {

				} else {
					Vector<Integer> ids = new Vector<Integer>();
					String[] split = eingabe.split(",");
					for (String s : split) {
						if (s.contains("-")) {
							String[] splitt = s.split("-");
							ids.add(Integer.parseInt(splitt[0]));
							ids.add(Integer.parseInt(splitt[1]));
						} else {
							ids.add(Integer.parseInt(s));
						}
					}

				}

			} catch (NumberFormatException e) {
				System.out.println("Fehler : Keine Zahl erkannt");
				return;
			}
			break;
		case "all":

			break;
		}
	}

	private void aufnahmen() {
		Vector<String> otrkeys = OTRDServer.getThreadManager().getOtrKeyFinder().getAufnahmeleser().getOtrkeys();
		System.out.println("--------------------------------------------------");
		for (String s : otrkeys) {
			System.out.println("\t" + s);
		}
		System.out.println("--------------------------------------------------");
	}

	/**
	 * Methode um verschiedene Informationen �ber vorhandene Connections in die
	 * Konsole auszugeben
	 * 
	 * @param type
	 *            Art der Information
	 */
	private void connection(int type) {
		switch (type) {
		case 0:
			Vector<Connection> cons = OTRDServer.getThreadManager().getDLServer().getConnections();
			System.out.println("--------------------------------------------------");
			int i = 0;
			for (Connection c : cons) {
				System.out.println(i + " - " + c.getIP() + " - " + c.getDate().toString());
				i++;
			}
			System.out.println("--------------------------------------------------");
			break;
		case 1:
			connection(0);
			System.out.println("Remove ID :");
			Scanner eingabe = new Scanner(System.in);
			String text = eingabe.nextLine();
			if (!text.contains("exit") && !text.equals("")) {
				int input = Integer.parseInt(text);
				OTRDServer.getThreadManager().getDLServer().getConnections().remove(input);
			}
			break;
		case 2:
			Vector<Connection> cons2 = OTRDServer.getThreadManager().getDLServer().getConnections();
			System.out.println("--------------------------------------------------");
			int u = 0;
			for (Connection c : cons2) {
				System.out.println(u + " - " + c.getIP() + " - " + c.getDate().toString());
				int z = 0;
				for (OtrKeyResult result : c.getOtrkeys()) {
					System.out.println("\t" + z + " - " + result.getOtrkey());
					z++;
				}
				u++;
			}
			System.out.println("--------------------------------------------------");
			break;
		}
	}

	/**
	 * Verschiedene Funktionen zu Manualkeys
	 * 
	 * @param type
	 *            Art der Funktion
	 */
	public void manualkey(int type) {
		switch (type) {
		case 1:
			System.out.print("OTRKEY : ");
			String text = scanner.nextLine();
			if (!text.contains("exit") && !text.equals("")) {
				OTRDServer.getThreadManager().getOtrKeyFinder().addManualkey(text);
			}
			break;
		}
	}

}
