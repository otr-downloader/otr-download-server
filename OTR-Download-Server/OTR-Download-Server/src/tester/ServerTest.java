package tester;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import core.main.OTRDServer;
import downloader.server.DLServer;

class ServerTest {

	@BeforeAll
	static void prepare() {
		new OTRDServer(true);
	}
	
	@Test
	void test() {
		DLServer dlServer = new DLServer();
		assertEquals(dlServer.getConnections().size(), 0);
		Thread thread = new Thread(dlServer);
		thread.start();
	}

}
