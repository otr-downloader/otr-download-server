package finder.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import core.main.Debug;
import core.main.Logger;
import core.main.OTRDServer;

public class OTRKeyFinder implements Runnable{
	
	private Aufnahmenleser aufnahmenleser;
	
	private Vector<String> manualkeys;
	
	public OTRKeyFinder() {
		aufnahmenleser = new Aufnahmenleser();
		manualkeys = new Vector<String>();
		System.setProperty("javax.net.ssl.trustStore","truststore");
	    System.setProperty("javax.net.ssl.trustStorePassword", "311771909");
	}

	/**
	 * Sucht nach Mirros die den angegebenen OTRKEY zum Download anbieten
	 * @param otrkey OTRKEY
	 * @return OtrKeyResult oder null falls es kein Ergebnis gibt
	 */
	public OtrKeyResult search(String otrkey) {
		try {
			URL url = new URL("https://otrkeyfinder.com/de/?search="+otrkey);
			Debug.println("Lese URL: "+url+" aus");
			
			URLConnection urlConnection = url.openConnection();
			urlConnection.setDoInput(true);
			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
			
			String line = null;
			String lines = null;
			
			while((line = reader.readLine())!= null) {
				lines += line;
			}
			
			//Falls ein nichterlaubtes Zeichen oder es keine Ergebnisse gibt wird null zur�ckgegeben
			if(lines.contains("Suche fehlgeschlagen") || lines.contains("0 otrkey-Dateien gefunden")) {
				return null;
			}
			
			String split = lines.split("<div class=\"box-content\">")[1];
			String[] otrkeys = split.split("\" class=\"otrkey\">");
			if(otrkeys.length == 1)
				otrkeys = split.split("\" class=\"otrkey active\">");
			
			HashMap<Integer, Vector<Vector<String>>> results = new HashMap<Integer, Vector<Vector<String>>>();
			HashMap<Integer, String> cotrkeys = new HashMap<Integer, String>();
			
			for(int i = 1;i<otrkeys.length;i++) {
				String s = otrkeys[i];
				String ckey = s.split("<span class=\"file\">")[1].split("</span>")[0];
				int format = format(s.split("<span class=\"format ")[1].split("\"")[0]);
				String tmp = s.split("<ul class=\"mirror-list\"")[1].split("</ul>")[0];
				Vector<Vector<String>> vecformat = new Vector<Vector<String>>();
				String [] arrayli = tmp.split("<li>");
				for(int y = 1;y<arrayli.length;y++) {
					String li = arrayli[y];
					String gmirror = li.split("<div class=\"mirror\">")[1];
					String mirror = gmirror.split(">")[1].split("</a>")[0];
					String link = gmirror.split("<a href=\"")[1].split("\"")[0];
					Vector<String> tmpvec = new Vector<String>();
					tmpvec.add(mirror);
					tmpvec.add(escapeHtml(link));
					vecformat.add(tmpvec);
				}
				cotrkeys.put(format, ckey);
				results.put(format, vecformat);
			}
			
			int bformat = bestformat(results);
			
			OtrKeyResult rKeyResult = getResult(results.get(bformat), bformat);
			rKeyResult.setOtrkey(cotrkeys.get(bformat));
			rKeyResult.setQualitaet(bformat);
			
			return rKeyResult;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Vector<OtrKeyResult> search(Vector<String> otrkeys){
		Vector <OtrKeyResult> keyResults = new Vector<>();
		for(String s : otrkeys) {
			keyResults.add(search(s));
		}
		return keyResults;
	}
	
	public Vector<OtrKeyResult> search(String searchtext, Vector<String> ignoreKeys){		
		try {
			URL url = new URL("https://otrkeyfinder.com/de/?search="+searchtext);
			Debug.println("Lese URL: "+url+" aus");
			
			URLConnection urlConnection = url.openConnection();
			urlConnection.setDoInput(true);
			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
			
			String line = null;
			String lines = null;
			
			while((line = reader.readLine())!= null) {
				lines += line;
			}
			
			//Falls ein nichterlaubtes Zeichen oder es keine Ergebnisse gibt wird null zur�ckgegeben
			if(lines.contains("Suche fehlgeschlagen") || lines.contains("0 otrkey-Dateien gefunden")) {
				return null;
			}
			
			String split = lines.split("<div class=\"box-content\">")[1];
			String[] otrkeys = split.split("\" class=\"otrkey\">");
			if(otrkeys.length == 1)
				otrkeys = split.split("\" class=\"otrkey active\">");
			
			class FinderResult {
				
				private HashMap<Integer, Vector<Vector<String>>> results = new HashMap<Integer, Vector<Vector<String>>>();
				private HashMap<Integer, String> cotrkeys = new HashMap<Integer, String>();
			}
			
			HashMap<String, FinderResult> fresults = new HashMap<>();
			
			for(int i = 1;i<otrkeys.length;i++) {
				String s = otrkeys[i];
				String ckey = s.split("<span class=\"file\">")[1].split("</span>")[0];
				if(ignoreKeys != null && ignoreKeys.contains(neutralize(ckey)))
					continue;
				
				int format = format(s.split("<span class=\"format ")[1].split("\"")[0]);
				String tmp = s.split("<ul class=\"mirror-list\"")[1].split("</ul>")[0];
				Vector<Vector<String>> vecformat = new Vector<Vector<String>>();
				String [] arrayli = tmp.split("<li>");
				for(int y = 1;y<arrayli.length;y++) {
					String li = arrayli[y];
					String gmirror = li.split("<div class=\"mirror\">")[1];
					String mirror = gmirror.split(">")[1].split("</a>")[0];
					String link = gmirror.split("<a href=\"")[1].split("\"")[0];
					Vector<String> tmpvec = new Vector<String>();
					tmpvec.add(mirror);
					tmpvec.add(escapeHtml(link));
					vecformat.add(tmpvec);
				}
				String nkey = neutralize(ckey);
				if(!fresults.containsKey(nkey)) {
					FinderResult finderResult = new FinderResult();
					
					finderResult.cotrkeys.put(format, ckey);
					finderResult.results.put(format, vecformat);
					
					continue;
				}
				
				FinderResult finderResult = fresults.get(nkey);
				finderResult.cotrkeys.put(format, ckey);
				finderResult.results.put(format, vecformat);
			}
			
			Vector<OtrKeyResult> oresults = new Vector<>();
			
			for(FinderResult result : fresults.values()) {
				int bformat = bestformat(result.results);
				
				OtrKeyResult rKeyResult = getResult(result.results.get(bformat), bformat);
				rKeyResult.setOtrkey(result.cotrkeys.get(bformat));
				rKeyResult.setQualitaet(bformat);
				
				oresults.add(rKeyResult);
			}
			
			return oresults;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Vector<String> searchlistOtrKeys(String searchtext){
		try {
			
			int page = 1;
			Vector<String> keys = new Vector<>();
			HashMap<String, Integer> sformat = new HashMap<>();
			HashMap<String, String> stos = new HashMap<>();
			
			while(true) {
				System.out.println(page);
				URL url = new URL("http://otrkeyfinder.com/de/?search="+searchtext+"&page="+page);
				//Debug.println("Lese URL: "+url+" aus");
				
				URLConnection urlConnection = url.openConnection();
				urlConnection.setDoInput(true);
				BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
				
				String line = null;
				String lines = null;
				
				while((line = reader.readLine())!= null) {
					//OTRDServer.getLog().println(line);
					lines += line;
				}
				
				
				//OTRDServer.getLog().println("");
				
				//Falls ein nichterlaubtes Zeichen oder es keine Ergebnisse gibt wird null zur�ckgegeben
				if(lines.contains("Suche fehlgeschlagen") || lines.contains(" 0 otrkey-Dateien gefunden")) {
					return null;
				}
				
				
				
				String split = lines.split("<div class=\"box-content\">")[1];
				String[] otrkeys = split.split("\" class=\"otrkey\">");
				if(otrkeys.length == 1)
					otrkeys = split.split("\" class=\"otrkey active\">");
				
				
				for(int i = 1;i<otrkeys.length;i++) {
					String s = otrkeys[i];
					String ckey;
					if(s.contains("<span class=\"file\">"))
						ckey = s.split("<span class=\"file\">")[1].split("</span>")[0];
					else
						ckey = s.split("<span class=\"file no-mirrors\">")[1].split("</span>")[0];
					int format = format(s.split("<span class=\"format ")[1].split("\"")[0]);
					String neutral = neutralize(ckey);
					
					if(sformat.getOrDefault(neutral, -1) < format) {
						sformat.put(neutral, format);
						stos.put(neutral, ckey);
					}
				}
				
				if(!lines.contains("<li class=\"next\">") || lines.contains("<li class=\"next disabled\">")) {
					break;
				}
				page++;
			}
			
			
			
			for(String s : stos.values()) //TODO: Sortierung
				keys.add(s);
			
			return keys;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int format(String format) {
		switch(format) {
		case "mp4":
			return 0;
		case "avi":
			return 1;
		case "hq":
			return 2;
		case "hd":
			return 3;
		}
		return -1;
	}
	
	public static int formatOTRKey(String otrkey) {
		if(otrkey.contains("HD"))
			return 3;
		else if(otrkey.contains("HQ"))
			return 2;
		else if(otrkey.contains("avi"))
			return 1;
		else if(otrkey.contains("mp4"))
			return 0;
		return -1;
	}
	
	private int bestformat(HashMap<Integer, Vector<Vector<String>>> results) {
		if(results.containsKey(3)) {
			return 3;
		}else if(results.containsKey(2)) {
			return 2;
		}else if(results.containsKey(1)) {
			return 1;
		}else if(results.containsKey(0)) {
			return 0;
		}
		
		return -1;
	}
	
	private OtrKeyResult getResult(Vector<Vector<String>> vecresults, int format) {
		OtrKeyResult rOtrKeyResult = new OtrKeyResult();
		for(Vector<String> vec : vecresults) {
			if(vec.elementAt(0).contains("otr.datenkeller.net")) {
				rOtrKeyResult.setDatenkeller(vec.elementAt(1));
			}else if(vec.elementAt(0).contains("dlmirror.eu")) {
				rOtrKeyResult.setDlmirror(vec.elementAt(1));
			}else if(vec.elementAt(0).contains("otr-ratte.de")) {
				rOtrKeyResult.setOtrratte(vec.elementAt(1));
			}
		}
		
		if(rOtrKeyResult.getDatenkeller() == null && rOtrKeyResult.getDlmirror() == null && rOtrKeyResult.getDlmirror() == null)
			return null;
		
		rOtrKeyResult.setQualitaet(format);
		
		return rOtrKeyResult;
	}
	
	public static String neutralize(String otrkeyname){
		String name = "";
		String[] split = otrkeyname.split("\\.");
		for (int i = 0; i < split.length; i++) {
			switch (split[i]) {
			case "mpg":
			case "avi":
			case "HQ":
			case "HD":
			case "mp4":
			case "ac3":
			case "otrkey":
				break;
			default:
				name = name.concat(split[i] + ".");
				break;
			}
		}
		return name;
	}
	
	private String escapeHtml(String s) {
		return s.replace("&amp;", "&");
	}

	@Override
	public void run() {
		while(true) {
			//Debug.println("Otrkeysuche l�uft");
			Vector<OtrKeyResult> results = new Vector<OtrKeyResult>();
			
			Vector<String> otrkeys;
			try {
				otrkeys = aufnahmenleser.update();
			}catch(NullPointerException e) {
				e.printStackTrace(OTRDServer.getLog().getWriter());
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				continue;
			}
			Vector<String> mkeys = new Vector<String>(manualkeys);
			
			for(String okey : otrkeys) {
				try {
					OtrKeyResult result = search(neutralize(okey));
					if(result == null) {
						continue;
					}
					results.add(result);
				} catch (Exception e) {
					e.printStackTrace(OTRDServer.getLog().getWriter());
					e.printStackTrace();
				}
			}
			
			for(String okey : mkeys){
				OtrKeyResult result = search(neutralize(okey));
				if(result == null) {
					OTRDServer.getLog().println(okey+" aus Manualkeys entfernt, da da es 0 Ergebnisse gab oder ein illegalles Zeichen verwendet wurde");
					System.out.println("FEHLER : "+okey+" aus Manualkeys entfernt!");
					manualkeys.remove(okey);
					continue;
				}
				results.add(result);
				manualkeys.remove(okey);
			}
			
			for(OtrKeyResult keyResult : results) {
				Debug.println(keyResult.asString());
				OTRDServer.getThreadManager().getDownloadManager().addDownload(keyResult);
			}
			
			//CPU Entlastung
			try {
				Thread.sleep(120000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Date getLastCheck() {
		return aufnahmenleser.getLCheck();
	}
	
	public void addManualkey(String mkey) {
		manualkeys.add(mkey);
	}
	
	public Aufnahmenleser getAufnahmeleser() {
		return aufnahmenleser;
	}
	
}
