package finder.adv;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

public class Suchmuster {

	private String Senderpattern, Datumpattern, Startzeitpattern, Titel, Staffelpattern, Folgepattern, Titelpattern;
	
	public Suchmuster() {
		Senderpattern = Datumpattern = Startzeitpattern = Folgepattern = Staffelpattern = setTitelpattern(".*");
	}
	
	public boolean equalsSuchmuster(String otrkey) {
		if(!isComplete())
			return false;
		
		String[] split = otrkey.split("_");
		int size = split.length;
		
		String sender = split[size-4], startzeit = split[size-5], startdatum = split[size-6];
		String titel = "";
		int last = 7;
		String staffel = null, folge = null;
		
		if(Pattern.matches("S\\d*E\\d*", split[size-last])) {
			folge = split[size-last].split("E")[1];
			staffel = split[size-last].split("S")[1].split("E")[0];
			last = 8;
		}
		
		for(int i = 0; i<size -last;i++)
			titel += split[i];
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy.MM.dd");
		Date d;
		try {
			d = dateFormat.parse(startdatum);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		SimpleDateFormat format = new SimpleDateFormat("u", Locale.GERMAN);
		
		
		if(Pattern.matches(Titelpattern, titel) && Pattern.matches(Startzeitpattern, startzeit) &&
				Pattern.matches(Datumpattern, format.format(d)) && Pattern.matches(Senderpattern, sender) && 
				(((Folgepattern.equals(".*") && Staffelpattern.equals(".*")) || (staffel == null && folge == null)) || 
						(Pattern.matches(Folgepattern, folge) && Pattern.matches(Staffelpattern, staffel))))
					return true;
		
		return false;
	}
	
	private boolean isComplete() {
		if(getDatumpattern() != null && getSenderpattern() != null && getStartzeitpattern() != null && getTitel() != null && getTitelpattern() != null)
			return true;
		return false;
	}

	public String getSenderpattern() {
		return Senderpattern;
	}

	public void setSenderpattern(String senderpattern) {
		Senderpattern = senderpattern;
	}

	public String getDatumpattern() {
		return Datumpattern;
	}

	/**
	 * Pattern f�r die Zahl des Wochentags
	 * @param datumpattern
	 */
	public void setDatumpattern(String datumpattern) {
		Datumpattern = datumpattern;
	}

	public String getStartzeitpattern() {
		return Startzeitpattern;
	}

	public void setStartzeitpattern(String startzeitpattern) {
		Startzeitpattern = startzeitpattern;
	}

	public String getTitel() {
		return Titel;
	}

	public boolean setTitel(String titel) {
		if(titel.length() < 3)
			return false;
		Titel = titel;
		return true;
	}

	public String getStaffelpattern() {
		return Staffelpattern;
	}

	public void setStaffelpattern(String staffelpattern) {
		Staffelpattern = staffelpattern;
	}

	public String getFolgepattern() {
		return Folgepattern;
	}

	public void setFolgepattern(String folgepattern) {
		Folgepattern = folgepattern;
	}

	public String getTitelpattern() {
		return Titelpattern;
	}

	public String setTitelpattern(String titelpattern) {
		Titelpattern = titelpattern;
		return titelpattern;
	}
	
	
	
}
