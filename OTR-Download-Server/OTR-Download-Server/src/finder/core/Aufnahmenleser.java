package finder.core;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import core.main.Debug;
import core.main.OTRDServer;

public class Aufnahmenleser {

	private Vector<String> otrkeys;
	
	private long Aufnahmenpuffer = 54000000;
	
	private Date lastCheck;
	
	public Aufnahmenleser(){
		otrkeys = new Vector<String>();
		lastCheck = new Date();
	}
	
	/**
	 * Aktualisiert den Vektor mit den aktullen downloadbereiten Aufnahmen
	 */
	public Vector<String> update(){
		try {
			SyndFeedInput input = new SyndFeedInput();
			
			SyndFeed feed = input.build(new XmlReader(new URL("http://www.onlinetvrecorder.com/rss/my2.php?user_id=2450312&hash=e67be5c5bdb4cc7d78720bdfdb75191f")));
			
			
			@SuppressWarnings("unchecked")
			Vector<SyndEntry> entries = new Vector<SyndEntry>(feed.getEntries()) ;
			
			Vector<String> names = new Vector<String>();
			
			Vector<String> otrkeys = new Vector<String>();
			
			Vector<String> hdsender = OTRDServer.getConf().DateiAuslesen(System.getProperty("user.dir")+File.separator+"config"+File.separator+"hdsender.txt");
			
			for(SyndEntry e : entries){
				String name = "";
				for(String i : e.getTitle().split("\\.")){
					switch(i){
					case "mpg":
					case "avi":
					case "HQ":
					case "HD":
					case "mp4":
					case "ac3":
					case "mp3":
					case "fra":
					case "otrkey":
					break;
					default:
						name = name.concat(i+".");
						break;
					}
				}
				Pattern p = Pattern.compile("_..\\...\\..._..-.._");
				Matcher m = p.matcher(name);
				DateFormat dateformat = new SimpleDateFormat("_yy.MM.dd_HH-mm_");
				
				try {
					for(String s : hdsender){
						if(name.contains(s)){
							if(m.find() && !dateformat.parse(m.group(0)).before(new Date(System.currentTimeMillis()-72000000))){
								OTRDServer.getLog().println(name + " = HD Sender warte auf Verf�gbarkeit");
								continue;
							}
						}
					}
					if(m.find() && !dateformat.parse(m.group(0)).before(new Date(System.currentTimeMillis()-Aufnahmenpuffer))){
						OTRDServer.getLog().println(name+" nicht verf�gbar");
						continue;
					}
				} catch (ParseException e1) {
					OTRDServer.getLog().println("PARSE ERROR : "+name+" & "+m.group(0));
					e1.printStackTrace(OTRDServer.getLog().getWriter());
					continue;
				}
				if(!names.contains(name)){
					names.add(name);
					otrkeys.add(e.getTitle());
				}else if(names.contains(name) && qualitaet(otrkeys.elementAt(names.indexOf(name))) < qualitaet(e.getTitle())){
					otrkeys.remove(names.indexOf(name));
					names.remove(name);
					names.add(name);
					otrkeys.add(e.getTitle());
				}
			}
			
			Vector<String> unique = new Vector<String>();
			
			for(String s : otrkeys) {
				if(!this.otrkeys.contains(s)) {
					unique.add(s);
				}
			}
			
			this.otrkeys.addAll(unique);
			
			lastCheck = new Date();
			
			return unique;
			
		} catch (ConnectException e){
			OTRDServer.getLog().println("OTR Aufnahmenleser : Connection Exception "+e.getMessage()); 
			Debug.println("OTR Aufnahmenleser : Connection Exception "+e.getMessage());
		}catch (SocketException e) {
			Debug.println("OTR Aufnahmenleser : Socket Exception "+e.getMessage());
			OTRDServer.getLog().println("OTR Aufnahmenlesser : Socket Exception "+e.getMessage());
		}catch (UnknownHostException e) {
			Debug.println("OTR Aufnahmenleser : Unknown Host "+e.getMessage());
			OTRDServer.getLog().println("OTR Aufnahmenleser : Unknown Host "+e.getMessage());
		}catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (FeedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new Vector<String>();
	}
	
	private int qualitaet(String otrkeyname){
		if(otrkeyname.contains("HD.avi")){
			return 4;
		}else if(otrkeyname.contains("HQ.avi")){
			return 3;
		}else if(otrkeyname.contains("avi")){
			return 2;
		}else if(otrkeyname.contains("mp4")){
			return 1;
		}else{
			return 0;
		}
	}
	
	/**
	 * Gibt die Zeit des letzten erfolgreichen Aufnahmenchecks zur�ck
	 * @return
	 */
	public Date getLCheck() {
		return lastCheck;
	}

	/**
	 * Gibt die OTRKeys der Aufnahmen die bereits abgerufen wurden zur�ck
	 * @return
	 */
	public Vector<String> getOtrkeys() {
		return otrkeys;
	}
}
