package finder.core;

public class OtrKeyResult {

	private String otrkey;
	private int qualitaet;
	
	private String datenkeller;
	private String dlmirror;
	private String otrratte;

	public String getOtrkey() {
		return otrkey;
	}

	public void setOtrkey(String otrkey) {
		this.otrkey = otrkey;
	}

	public int getQualitaet() {
		return qualitaet;
	}

	public void setQualitaet(int qualitaet) {
		this.qualitaet = qualitaet;
	}

	public String getDatenkeller() {
		return datenkeller;
	}

	public void setDatenkeller(String datenkeller) {
		this.datenkeller = datenkeller;
	}

	public String getDlmirror() {
		return dlmirror;
	}

	public void setDlmirror(String dlmirror) {
		this.dlmirror = dlmirror;
	}

	public String getOtrratte() {
		return otrratte;
	}

	public void setOtrratte(String otrratte) {
		this.otrratte = otrratte;
	}
	
	public boolean NotNull(){
		if(datenkeller != null){
			return true;
		}
		if(dlmirror != null){
			return true;
		}
		if(otrratte != null){
			return true;
		}
		return false;
	}
	
	public String asString() {
		return "otrkeyresult!"+otrkey+"!"+qualitaet+"!"+datenkeller+"!"+dlmirror+"!"+otrratte+"!";
	}
	
	public static OtrKeyResult getResult(String s) {
		OtrKeyResult keyResult = new OtrKeyResult();
		String[] split = s.split("!");
		if(split[0].equals("otrkeyresult")) {
			keyResult.setOtrkey(split[1]);
			keyResult.setQualitaet(Integer.parseInt(split[2]));
			keyResult.setDatenkeller(split[3]);
			keyResult.setDlmirror(split[4]);
			keyResult.setOtrratte(split[5]);
			return keyResult;
		}else {
			System.out.println("Kein g�ltiges OtrKeyResult Format");
			return null;
		}
	}
	
}
