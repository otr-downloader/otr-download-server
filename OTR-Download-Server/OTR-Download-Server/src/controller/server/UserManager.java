package controller.server;

import java.io.File;
import java.io.IOException;

public class UserManager {

	File savefile;

	public UserManager() {
		savefile = new File(
				System.getProperty("user.dir") + File.separator + "config" + File.separator + "usermanager.txt");
		if (!savefile.exists()) {
			try {
				savefile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
