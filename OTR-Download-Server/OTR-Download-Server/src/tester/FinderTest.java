package tester;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Vector;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import core.main.OTRDServer;
import finder.core.OTRKeyFinder;
import finder.core.OtrKeyResult;

class FinderTest {
	
	@BeforeAll
	static void prepare() {
		new OTRDServer(true);
	}
	
	@Test
	void testFinderOneResult() {
		OTRKeyFinder finder = new OTRKeyFinder();
		OtrKeyResult keyResult = finder.search("");
		assertNotEquals(null, keyResult);
	}
	
	@Test
	void testFinderMultiResult() {
		OTRKeyFinder finder = new OTRKeyFinder();
		Vector<String> strings = new Vector<>();
		strings.add("18.06.24_23-15_sat1_");
		strings.add("Godzilla_18.06.24_20-15_pro7_150_TVOON_DE.mpg");
		Vector<OtrKeyResult> keyResult = finder.search(strings);
		assertNotEquals(null, keyResult);
		assertEquals(strings.size(), keyResult.size());
	}

}
